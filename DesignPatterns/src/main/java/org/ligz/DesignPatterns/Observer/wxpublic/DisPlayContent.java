package org.ligz.DesignPatterns.Observer.wxpublic;

/**
 * 观察者模式
 * 为用户建立一个共同的接口，实现展示的功能
 * @author ligz
 */
public interface DisPlayContent {
	public void display();
}
