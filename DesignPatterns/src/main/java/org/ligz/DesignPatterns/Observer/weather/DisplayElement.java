package org.ligz.DesignPatterns.Observer.weather;

public interface DisplayElement {
	
	public void display();
	
}
