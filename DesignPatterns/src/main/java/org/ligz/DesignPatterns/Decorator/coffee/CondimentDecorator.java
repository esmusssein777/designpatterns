package org.ligz.DesignPatterns.Decorator.coffee;

public abstract class CondimentDecorator extends Beverage{
	
	public abstract String getDescription();
	
}
