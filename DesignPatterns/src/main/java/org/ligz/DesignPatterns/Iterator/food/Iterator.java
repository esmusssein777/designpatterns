package org.ligz.DesignPatterns.Iterator.food;

/**
 * @author ligz
 */
public interface Iterator {
	boolean hasNext();
	
	Object next();
}
