package org.ligz.DesignPatterns.Template.coffee;

/**
 * 测试方法
 * @author ligz
 */
public class Test {
	public static void main(String[] args) {
		Tea tea = new Tea();
		tea.prepareRecipe();
	}
}
