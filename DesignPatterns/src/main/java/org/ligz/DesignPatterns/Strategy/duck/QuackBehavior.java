package org.ligz.DesignPatterns.Strategy.duck;

/**
 * 接口
 * @author ligz
 */
public interface QuackBehavior {
	public void quack();
}
